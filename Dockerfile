FROM registry.fedoraproject.org/fedora-minimal:37

LABEL   MAINTAINER=tob@butter.sh \
        io.openshift.tags=backup,restic \
        io.k8s.description="Backup with restic" \
        io.openshift.non-scalable=true

RUN microdnf install -y restic \
 && microdnf clean all

ENTRYPOINT ["/usr/bin/restic"]
CMD ["backup"]
